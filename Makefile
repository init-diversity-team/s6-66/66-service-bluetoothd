#
# This Makefile requires GNU make.
#
# Do not make changes here.
# Use the included .mak files.
#

make_need := 3.81
ifeq "" "$(strip $(filter $(make_need), $(firstword $(sort $(make_need) $(MAKE_VERSION)))))"
fail := $(error Your make ($(MAKE_VERSION)) is too old. You need $(make_need) or newer)
endif

-include config.mak
include package/targets.mak

LOWDOWN := $(shell type -p lowdown)
ifdef LOWDOWN
GENERATE_HTML := $(shell doc/make-html.sh)
GENERATE_MAN := $(shell doc/make-man.sh)
endif
INSTALL_HTML := $(wildcard doc/html/*.html)
INSTALL_MAN := $(wildcard doc/man/*/*)
INSTALL := ./tools/install.sh

clean:
	@exec rm -rf $(DESTDIR)

install: install-service install-user-service install-html install-man
install-service: $(SERVICE_TARGET:service/%=$(DESTDIR)$(service_directory)/%)
install-user-service: $(SERVICE_USER_TARGET:service/user/%=$(DESTDIR)$(service_directory)/user/%)
install-html: $(INSTALL_HTML:doc/html/%.html=$(DESTDIR)$(datarootdir)/doc/$(package_doc)/%.html)
install-man: install-man1
install-man1: $(INSTALL_MAN:doc/man/man1/%.1=$(DESTDIR)$(mandir)/man1/%.1)

$(DESTDIR)$(service_directory)/%: service/%
	exec $(INSTALL) -D -m 644 $< $@
	exec sed -e "s/@BINDIR@/$(subst /,\/,$(bindir))/g" \
			-e "s/@SYSCONFDIR@/$(subst /,\/,$(sysconfdir))/g" \
			-e "s/@DATAROOTDIR@/$(subst /,\/,$(datarootdir))/g" \
			-e "s/@LIVEDIR@/$(subst /,\/,$(livedir))/g" \
			-e "s/@SERVICE_DIRECTORY@/$(subst /,\/,$(service_directory))/g" \
			-e "s/@SCRIPT_DIRECTORY@/$(subst /,\/,$(script_directory))/g" \
			-e "s/@SEED_DIRECTORY@/$(subst /,\/,$(seed_directory))/g" \
			-e "s/@SERVICE_CONF_DIRECTORY@/$(subst /,\/,$(adm_conf))/g" \
			-e "s/@SKEL_DIRECTORY@/$(subst /,\/,$(skel_directory))/g" \
			-e "s/@EXECLINE_SHEBANGPREFIX@/$(subst /,\/,$(shebangdir))/g" \
			-e "s/@VERSION@/$(subst /,\/,$(version))/g"  $< > $@

$(DESTDIR)$(service_directory)/user/%: service/user/%
	exec $(INSTALL) -D -m 644 $< $@
	exec sed -e "s/@BINDIR@/$(subst /,\/,$(bindir))/g" \
			-e "s/@SYSCONFDIR@/$(subst /,\/,$(sysconfdir))/g" \
			-e "s/@DATAROOTDIR@/$(subst /,\/,$(datarootdir))/g" \
			-e "s/@LIVEDIR@/$(subst /,\/,$(livedir))/g" \
			-e "s/@SERVICE_DIRECTORY@/$(subst /,\/,$(service_directory))/g" \
			-e "s/@SCRIPT_DIRECTORY@/$(subst /,\/,$(script_directory))/g" \
			-e "s/@SEED_DIRECTORY@/$(subst /,\/,$(seed_directory))/g" \
			-e "s/@SERVICE_CONF_DIRECTORY@/$(subst /,\/,$(adm_conf))/g" \
			-e "s/@SKEL_DIRECTORY@/$(subst /,\/,$(skel_directory))/g" \
			-e "s/@EXECLINE_SHEBANGPREFIX@/$(subst /,\/,$(shebangdir))/g" \
			-e "s/@VERSION@/$(subst /,\/,$(version))/g"  $< > $@

$(DESTDIR)$(datarootdir)/doc/$(package_doc)/%.html: doc/html/%.html
	$(INSTALL) -D -m 644 $< $@ && \
	sed -e 's,%%skel_directory%%,$(skel_directory),g' $< > $@

$(DESTDIR)$(mandir)/man1/%.1: doc/man/man1/%.1
	$(INSTALL) -D -m 644 $< $@ && \
	sed -e 's,%%skel_directory%%,$(skel_directory),g' $< > $@

version:
	@echo $(version)

.PHONY: clean install install-service install-user-service version install-html install-man

.DELETE_ON_ERROR:
